package com.sample.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.sample.util.TokenValidator;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@Component
public class JwtAuthenticationFilterOnce extends OncePerRequestFilter {

	@Autowired
	private TokenValidator tokenValidate;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String header = request.getHeader("Authorization");
		Map<String, Object> errorResponse = new HashMap<String, Object>();

		String apiUrl =request.getRequestURI();

        if (header == null || !header.startsWith("Bearer ") || apiUrl.contains("/public/")) {
        	
            filterChain.doFilter(request, response); // If no token found and call from public API, continue with the next filter
            
            return;
        }
        
        String token = header.replace("Bearer ", "");

		boolean isvalid = false;

		try {
			isvalid = tokenValidate.validateToken(token,request);

			if(isvalid) {

				// username to be passed of use name of SUB from auth token
				Authentication authentication = new UsernamePasswordAuthenticationToken("username", null, null);

				SecurityContextHolder.getContext().setAuthentication(authentication);
				filterChain.doFilter(request, response);
			}

		} catch (Throwable e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			errorResponse.put("message",e.getMessage());
			errorResponse.put("code",HttpStatus.UNAUTHORIZED.value());
			response.getWriter().write(new Gson().toJson(errorResponse));
		}
	}
}
