package com.sample.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {


	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {

		ObjectMapper objectMapper = new ObjectMapper();

		System.out.println(authException);
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		Map<String, Object> errorResponse = new HashMap<>();
		errorResponse.put("status", 401);

		if(authException instanceof InvalidBearerTokenException){
			System.out.println("In Bearer Error");
			errorResponse.put("message",((InvalidBearerTokenException) authException).getMessage());
		}else{
			System.out.println("ELSE Error: "+ authException.getClass());
			errorResponse.put("message", "something else:" + authException);
		}

		try (PrintWriter writer = response.getWriter()) {
			objectMapper.writeValue(writer, errorResponse);
		}
	}
}