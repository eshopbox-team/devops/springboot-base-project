package com.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsbSampleEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsbSampleEngineApplication.class, args);
	}

}
