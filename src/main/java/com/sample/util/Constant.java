package com.sample.util;

public class Constant {

    public static final String AUTH0_ID = "auth0id";
    public static final String ACCOUNT_SLUG = "account_slug";
    public static final String USER_TYPE = "userType";

    public static final String AUTH_ACCOUNT_SLUG = "auth";
    public static final String WAREHOUSE_ID = "warehouse_id";
    public static final String ACCOUNT_ID = "account_id";

    public static final Long ESHOPBOX_ACCOUNT_ID = 5L;

    public static final String WAREHOUSE_ACCOUNT_SLUG = "warehouse";
    public static final String EXT_WAREHOUSE_ID = "ext_warehouse_id";
    public static final String ROLE = "role";
    public static final String USER_ID = "user_id";
    public static final int _24_HOUR = 86400;

    public static final String USER_EMAIL = "USER_EMAIL";
}
