package com.sample.util;

import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CommonUtils {

    public static Timestamp getCurrentTime() {
        Timestamp timestamp = Timestamp.valueOf(ZonedDateTime
                .now(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
        System.out.println(timestamp);
        return timestamp;
    }

    public static String getValidString(String str) {
        return StringUtils.isBlank(str) ? null : str.trim();
    }

}
