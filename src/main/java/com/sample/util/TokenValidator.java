package com.sample.util;

import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.sample.model.wms.Account;
import com.sample.model.wms.UserAccount;
import com.sample.service.serviceImpl.AccountServiceImpl;
import com.sample.service.serviceImpl.UserAccountServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.sample.model.wms.User;
import com.sample.service.serviceImpl.UserServiceImpl;
import io.micrometer.common.util.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TokenValidator {
	private static final Logger logger = Logger.getLogger(TokenValidator.class.getName());
	@Autowired
	UserServiceImpl userService;

	@Autowired
	AccountServiceImpl accountService;

	@Autowired
	UserAccountServiceImpl userAccountService;

	@Value("${auth0.configIssuer}")
	private String configIssuer;
	@Value("${auth0.configClientId}")
	String configClientId;

	@Value("${auth0.configAudience}")
	String configAudience;
	
	public Map<String, Object> decodeToken(String jwtToken) throws Exception {
		
		Map<String, Object> response = new HashMap<String, Object>();
		
		try {
			String[] split_string = jwtToken.split("\\.");
			
			if (split_string.length == 3) {
				
				String base64EncodedHeader = split_string[0];
				String base64EncodedBody = split_string[1];
				String base64EncodedSignature = split_string[2];
				Base64 base64Url = new Base64(true);
				String body = new String(base64Url.decode(base64EncodedBody));

				StringBuffer tokenBody = new StringBuffer(body);
				response = new Gson().fromJson(tokenBody.toString(),
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				
				System.out.println(response);
				
			} else {
				throw new InvalidBearerTokenException("401:invalid Token");
			}

		}catch(Exception e) {
			
			throw new InvalidBearerTokenException("Token not valid: " + e.getMessage());
		}
		
		return response;
	}
	
	
	public boolean validateToken(String token, HttpServletRequest request) throws Exception {
		
		boolean response = false;

		try {

			Map<String, Object> decodeToken = this.decodeToken(token);
			
			// validate config details
			if(StringUtils.isNotBlank(decodeToken.toString())) {
				
				String oauthUserId = decodeToken.get("sub").toString();
				String tokenIssuer = decodeToken.get("iss").toString();
				String oauthClientId = decodeToken.get("azp").toString();
				ArrayList<String> oauthAudience = (ArrayList<String>) decodeToken.get("aud");
				Double oauthExpTime = (Double) decodeToken.get("exp");
				
				 if (isExpired(oauthExpTime)) {
					 
					 throw new InvalidBearerTokenException("Token is expired!");
					 
				 }
				 
				 if(configIssuer.equalsIgnoreCase(tokenIssuer) && configClientId.equalsIgnoreCase(oauthClientId) &&
						oauthAudience.contains(configAudience)) {
					 
					 // check for oauthUserId
					 User userDetails = userService.getUserByAuthId(oauthUserId);

					 if(null == userDetails) {
						 
						 throw new InvalidBearerTokenException("invalid Auth userss");

					 }else {

						 initializeCacheAndSession(request, decodeToken,userDetails);
						 // check use Account mapping
						 response = true;
					 }

				 }else {
					 
					 throw new InvalidBearerTokenException("invalid Auth0 Details For Env");
				 }
			}

		} catch (InvalidBearerTokenException e) {
			throw e; // Re-throw the custom exception for correct handling
		} catch (Exception e) {
			// Handle other unexpected exceptions
			throw new RuntimeException("Error validating token: " + e.getMessage());
		}
		return response;
	}
	
	public static boolean isExpired(Double expTime) {

        // Convert expiration time from seconds to milliseconds
		long expInMs = (long) (expTime * 1000);

		// Get current timestamp in milliseconds
		long now = Instant.now().toEpochMilli();

		// Calculate time remaining until expiration (in milliseconds)
		long timeRemaining = expInMs - now;

		// Check if token is expired
		return timeRemaining <= 0;
    }


	public void initializeCacheAndSession(HttpServletRequest request, Map<String, Object> decodeToken, User user){

		logger.info("initializing cache and session...");

		HttpSession session = request.getSession(true);

		session.setAttribute(Constant.ACCOUNT_SLUG, "");
		session.setAttribute(Constant.ACCOUNT_ID, 0L);
		logger.info("Memcache starting...");

		if(user != null) {
			session.setAttribute(Constant.AUTH0_ID, user.getAuth0Id());
			session.setAttribute(Constant.USER_ID, user.getId());
			session.setAttribute(Constant.USER_EMAIL, user.getEmail());
		}else {
			session.setAttribute(Constant.AUTH0_ID, 0L);
			session.setAttribute(Constant.USER_ID, 0L);
			session.setAttribute(Constant.USER_EMAIL, "");
		}

		System.out.println(decodeToken);

		String accountSlug = (String) decodeToken.get("https://accountSlug");

		List<String> warehouseList= (List<String>) decodeToken.get("https://warehouses");

		List<String> externalWarehouseIdList= (List<String>) decodeToken.get("https://warehouseWorkspaces");

		Map<String, Object> userMap= (Map<String, Object>) decodeToken.get("https://userDetails");

		logger.info("warehouseList : " + warehouseList);

		if(StringUtils.isBlank(accountSlug)) {

			  accountSlug = request.getHeader("ProxyHost").split("\\.")[0];

			  if (null != accountSlug || !accountSlug.isEmpty()) {
				  accountSlug = accountSlug.replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)", "");

			  }

			if(StringUtils.isBlank(accountSlug)) {
				throw new IllegalStateException("accountSlug is not found");
			}
		}

		// check for List<String> accountList= (List<String>) claimMap.get("https://accounts");
		List<String> accountList= (List<String>) decodeToken.get("https://accounts");

		if(accountList != null) {
			accountList = accountList.stream().map(accounts -> accounts.toLowerCase()).collect(Collectors.toList());
		}

		if(!accountList.contains(accountSlug)){
			System.out.println("Account Contains...");

			throw new IllegalStateException("User not have permission for Requested Account");
		}
		Account account = null;
		if (null != accountSlug) {

			session.setAttribute(Constant.ACCOUNT_SLUG, accountSlug);
			// getAccount by AccountSLug
			account = accountService.getAccountBySlug(accountSlug);
			session.setAttribute(Constant.ACCOUNT_SLUG, accountSlug);
			if (null != account) {
				session.setAttribute(Constant.ACCOUNT_ID, account.getId());
			}

		}

		String tokenUserType = (String) userMap.get("userType");
		session.setAttribute(Constant.USER_TYPE, tokenUserType);

		if("system".equalsIgnoreCase(tokenUserType)) {

			List<String> warehouseWorkspaceList = getWarehouseWorkspaceList();

			if(warehouseWorkspaceList !=null && warehouseWorkspaceList.contains(accountSlug)) {
				session.setAttribute("workspaceType", "warehouse");
			}
			else {
				session.setAttribute("workspaceType", "client");
			}
		}

		//ignore user account validation for user type as system
		if("system".equals(userMap.get("userType")) || accountSlug==null || "".equals(accountSlug))
			return;

		if (accountSlug.equalsIgnoreCase(Constant.AUTH_ACCOUNT_SLUG)) {
			session.setAttribute(Constant.ACCOUNT_SLUG, accountSlug);
			session.setAttribute(Constant.ACCOUNT_ID, (long)0);
			return;
		}

		if(externalWarehouseIdList !=null && externalWarehouseIdList.contains(accountSlug)) {
			session.setAttribute("workspaceType", "warehouse");
		}
		else {
			session.setAttribute("workspaceType", "client");
		}

		if(!accountSlug.equalsIgnoreCase(Constant.WAREHOUSE_ACCOUNT_SLUG)){
			logger.info("acountSLug_not_warehouse");

			if(accountList==null || accountList.size()==0 || !accountList.contains(accountSlug))
				throw new IllegalStateException("User do not have access to this account");
			if(StringUtils.isEmpty(accountSlug)) {

				appendAccountDetails(accountSlug,session,(String) userMap.get("email"));
			}else {
				String appAccountSlug = accountSlug;
				appendAccountDetailsForApp(appAccountSlug, session);
			}

		}else {
			logger.info("acountSlug_is_warehouse");
			String externalWarehouseId = request.getHeader("warehouseId");
			logger.info("externalWarehouseId :" + externalWarehouseId);
			if(externalWarehouseId==null || externalWarehouseId.equals("")) {
				session.setAttribute(Constant.WAREHOUSE_ID, (long)0);
				return;
			}
			if(warehouseList==null || warehouseList.size()==0 || !warehouseList.contains(externalWarehouseId)){
				throw new IllegalStateException("Access denied, You don't have permission for this warehouse.");
			}
		}
	}

	private List<String> getWarehouseWorkspaceList() {
		// TODO Auto-generated method stub
		List<String> warehouseWorkspaceList = null;
		try {

			warehouseWorkspaceList = accountService.getWarehouseWorkspaceList();
			System.out.println(warehouseWorkspaceList);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return warehouseWorkspaceList;

	}

	private void appendAccountDetails(String accountSlug, HttpSession httpSession, String userEmail) {
		// TODO Auto-generated method stub
		try {
			logger.info("httpSession.getAttribute(Constants.USER_ID) :" + (Long) httpSession.getAttribute(Constant.USER_ID));
			if(userEmail.contains("@eshopbox.com")) {

				Account account = accountService.getAccountBySlug(accountSlug);

				httpSession.setAttribute(Constant.ACCOUNT_ID, account.getId());
				httpSession.setAttribute(Constant.ROLE, "owner");
			} else {
				UserAccount userAccountMapping =userAccountService.getUserAccountMappingWithOutStatus(accountSlug, (Long) httpSession.getAttribute(Constant.USER_ID));

				httpSession.setAttribute(Constant.ACCOUNT_ID, userAccountMapping.getAccountId());
				httpSession.setAttribute(Constant.ROLE, userAccountMapping.getUserAccountRole());
			}

		}catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

	private void appendAccountDetailsForApp(String accountSlug, HttpSession httpSession) {
		// TODO Auto-generated method stub
		try {
			Account account = accountService.getAccountBySlug(accountSlug);

			httpSession.setAttribute(Constant.ACCOUNT_ID, account.getId());
			httpSession.setAttribute(Constant.ROLE, "");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

}
