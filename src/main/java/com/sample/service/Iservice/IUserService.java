package com.sample.service.Iservice;

import java.util.List;

import com.sample.bean.UserRequest;
import com.sample.model.wms.User;

public interface IUserService {


    public List<User> getAllUsers();
    
    public User getUserById(Long id);

    public void deleteUserById(Long id);

    public User createUser(UserRequest user);

    public User updateUser(Long id, UserRequest user);
    
    public User getUserByAuthId(String userAuthId);
    
}
