package com.sample.service.Iservice;

import com.sample.model.wms.Account;

import java.util.List;

public interface IAccountService {

    Account getAccountBySlug(String accountSlug);

    public List<String> getWarehouseWorkspaceList();

}
