package com.sample.service.Iservice;

import com.sample.model.wms.UserAccount;

public interface IUserAccountService {

    UserAccount getUserAccountMapping(Long user_id, Long account_id);

    UserAccount getUserAccountMappingWithOutStatus(String accountSlug, Long userId);
}
