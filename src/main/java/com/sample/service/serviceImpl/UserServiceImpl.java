package com.sample.service.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.sample.bean.UserRequest;
import com.sample.exception.ResourceNotFoundException;
import com.sample.model.wms.User;
import com.sample.repository.wms.UserRepository;
import com.sample.service.Iservice.IUserService;

@Service
@Component
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;
    @Override
    public List<User> getAllUsers() {

        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        Optional<User> userOpt = userRepository.findById(id);
        if(userOpt.isEmpty()){
        	
            throw new ResourceNotFoundException();
        }
        return userOpt.get();
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User createUser(UserRequest request) {
        User user = buildUser(request);
        return userRepository.save(user);
    }

    @Override
    public User updateUser(Long id, UserRequest request) {
        Optional<User> existingUserOpt = userRepository.findById(id);
        if(existingUserOpt.isEmpty()){
            throw new ResourceNotFoundException();
        }
        User user = updateUser(request, existingUserOpt.get());
        return userRepository.save(user);
    }

    private User buildUser(UserRequest request) {
    	
    	User user = new User();
    	
		return user;
    	
		/*
		 * return User.builder() .firstName(getValidString(request.getFirstName()))
		 * .lastName(getValidString(request.getLastName()))
		 * .address(getValidString(request.getAddress())) .contact(request.getContact())
		 * .email(getValidString(request.getEmail()))
		 * .username(getValidString(request.getUsername()))
		 * .password(getValidString(request.getPassword())) .createdAt(getCurrentTime())
		 * .updatedAt(getCurrentTime()) .build();
		 */
    }

    private User updateUser(UserRequest request, User user) {
    	
        return user;
    }
    
    @Override
    public User getUserByAuthId(String authoId) {
    	User user = new User();
    	
    	user = userRepository.findByAuth0Id(authoId);
    	
    	return user;
    }


}
