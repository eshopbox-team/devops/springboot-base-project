package com.sample.service.serviceImpl;

import com.sample.model.wms.Account;
import com.sample.repository.wms.AccountRepository;
import com.sample.service.Iservice.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class AccountServiceImpl implements IAccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public Account getAccountBySlug(String accountSlug) {

        Account account = accountRepository.findByAccountSlug(accountSlug);
        return account;
    }

    @Override
    public List<String> getWarehouseWorkspaceList() {

        List<String> accountList = accountRepository.getWarehouseWorkspaceAccount();

        return accountList;
    }
}
