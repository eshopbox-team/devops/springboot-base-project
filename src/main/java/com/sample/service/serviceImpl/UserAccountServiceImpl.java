package com.sample.service.serviceImpl;

import com.sample.model.wms.UserAccount;
import com.sample.repository.wms.UserAccountRepository;
import com.sample.service.Iservice.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
@Component
public class UserAccountServiceImpl implements IUserAccountService {

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public UserAccount getUserAccountMapping(Long userId, Long accountId) {

        UserAccount userAccount =userAccountRepository.findByUserIdAndAccountId(userId,accountId);

        return userAccount;
    }

    @Override
    public UserAccount getUserAccountMappingWithOutStatus(String accountSlug, Long userId) {

        UserAccount userAccount =userAccountRepository.getUserAccountMappingNative(accountSlug,userId);
        return userAccount;
    }
}
