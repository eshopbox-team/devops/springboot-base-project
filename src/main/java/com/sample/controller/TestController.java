package com.sample.controller;

import java.util.List;

import com.sample.util.Constant;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.bean.UserRequest;
import com.sample.model.wms.User;
import com.sample.service.Iservice.IUserService;

@RestController
@RequestMapping(path = "api", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
public class TestController {

    @Autowired
    IUserService userService;

    @GetMapping("/private/users")
    public ResponseEntity<List<User>> getAllUsers(HttpServletRequest httpRequest){

        HttpSession httpSession = httpRequest.getSession();
        System.out.println("in Controller...");
        System.out.println(httpSession.getAttribute(Constant.ACCOUNT_SLUG));

        System.out.println(httpSession.getAttribute(Constant.ACCOUNT_ID));
        System.out.println(httpSession.getAttribute(Constant.USER_EMAIL));

        System.out.println(httpSession.getAttribute(Constant.ROLE));
        System.out.println(httpSession.getAttribute(Constant.USER_TYPE));

        return ResponseEntity.ok(userService.getAllUsers());
    }


    @GetMapping("/wms/users/{id}")
    public ResponseEntity<User> getAllUsers(@PathVariable("id") Long id){
        return ResponseEntity.ok(userService.getUserById(id));
    }



    @PostMapping("/wms/users")
    public ResponseEntity<User> createUser(@RequestBody @Validated UserRequest user){
        return ResponseEntity.ok(userService.createUser(user));
    }

    @PutMapping("/wms/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody UserRequest user){
        return ResponseEntity.ok(userService.updateUser(id, user));
    }


    @DeleteMapping("/wms/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long id){
        userService.deleteUserById(id);
        return ResponseEntity.ok("User deleted successfully");
    }

}
