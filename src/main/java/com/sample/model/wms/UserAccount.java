package com.sample.model.wms;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "user_account_mapping")
public class UserAccount implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /** The user id. */
    @Column(name = "user_id")
    private Long userId;

    /** The account id. */
    @Column(name = "account_id")
    private Long accountId;

    /** The user account role. */
    @Column(name = "role")
    private String userAccountRole;

    /** The status. */
    @Column(name = "status")
    private String status;

    /** The preferences. */
    @Column(name = "preferences")
    private String preferences;

    /** The invitation url. */
    @Column(name = "invitationUrl")
    private String invitationUrl;

    @Column(name = "invitationFromUserId")
    private Long invitationFromUserId;

    /** The created at. */
    @Column(name = "created_at")
    private Date createdAt;

    /** The updated at. */
    @Column(name = "updated_at")
    private Date updatedAt;

    /** The email. */
    transient private String email;

}
