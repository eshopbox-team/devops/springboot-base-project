package com.sample.model.wms;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="verticals")
public class Verticals {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "vertical_name")
    private String verticalName;

    @Column(name = "vertical_code")
    private String verticalCode;

    @Column(name = "subdomain_prefix")
    private String subdomainPrefix;

    private Float vat;

    @Column(name = "main_image")
    private String mainImage;

    @Column(name = "hover_image")
    private String hoverImage;

    private Integer packetLimit;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Timestamp createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Timestamp updatedAt;


}
