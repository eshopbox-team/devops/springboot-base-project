package com.sample.model.wms;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="accounts")
public class Account implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /** The account name. */
    @Column(name = "account_name")
    private String accountName;

    /** The account slug. */
    @Column(name = "account_slug")
    private String accountSlug;

    /** The warehouseId. */
    @Column(name = "warehouseId")
    private Long warehouseId;

    /** The type. */
    // @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "enum")
    private String type;

    /** The status. */
    // @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "enum")
    private String status;

    /** The client code. */
    @Column(name = "client_code")
    private String clientCode;

    @Column(name = "partnerCode")
    private String partnerCode;


    /** The address line 1. */
    @Column(name = "address_line_1")
    private String addressLine1;

    /** The address line 2. */
    @Column(name = "address_line_2")
    private String addressLine2;

    /** The city. */
    @Column(name = "city")
    private String city;

    /** The state. */
    @Column(name = "state")
    private String state;

    /** The pincode. */
    @Column(name = "pincode")
    private Integer pincode;

    /** The country. */
    @Column(name = "country")
    private String country;

    /** The email. */
    @Column(name = "email")
    private String email;

    /** The phone. */
    @Column(name = "phone")
    private String phone;

    /** The pan number. */
    @Column(name = "pan_number")
    private String panNumber;

    /** The pancard scanned copy. */
    @Column(name = "pancard_scanned_copy", columnDefinition = "TEXT")
    private String pancardScannedCopy;

    /** The account number. */
    @Column(name = "account_no")
    private String accountNumber;

    /** The bank name. */
    @Column(name = "bank_name")
    private String bankName;

    /** The ifsc code. */
    @Column(name = "ifsc_code")
    private String ifscCode;

    /** The branch. */
    @Column(name = "branch")
    private String branch;

    /** The cancelled cheque scanned copy. */
    @Column(name = "cancelled_cheque_scanned_copy", columnDefinition = "TEXT")
    private String cancelledChequeScannedCopy;

    /** The contact person. */
    @Column(name = "contact_person")
    private String contactPerson;

    /** The gstin. */
    @Column(name = "gstin")
    private String gstin;

    /** The steps completed. */
    @Column(name = "stepsCompleted")
    private Byte stepsCompleted;

    @Column(name = "workspaceStatus")
    private String workspaceStatus;

    /** The created by. */
    @Column(name = "created_by")
    private String createdBy;

    /** The updated by. */
    @Column(name = "updated_by")
    private String updatedBy;


    /** The updated at. */
    @Column(name = "updated_at")
    private Date updatedAt;


    /** The created at. */
    @Column(name = "created_at")
    private Date createdAt;

    /** The contract url. */
    @Column(name = "contract_url", columnDefinition = "TEXT")
    private String contractUrl;

    @Column(name = "isOnTrialPeriod")
    private String isOnTrialPeriod;

    @Column(name = "workspace")
    private String workspace;

    @Column(name = "integration")
    private String integration;

    @Column(name = "orderItemLimit")
    private String orderItemLimit;

    @Column(name = "categories")
    private String categories;

    @Column(name = "inventoryLocation")
    private String inventoryLocation;

    transient private String workspaceType;

    transient private String externalWarehouseId;

}
