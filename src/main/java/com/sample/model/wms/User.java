package com.sample.model.wms;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="users")
public class User {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String lastName;

    private String email;

    @Column(name = "auth0id")
    private String auth0Id;

    private String phone;

    private String emailVerified;

    private String alternate_phone;
    
    private String office_phone;
    
    private String printer_settings;
    
    private String profileImageUrl;
    
    private String lastLogin;
    
    private String userType;
    
    private String companyName;
    
    private String subscriptionPlanId;
    
    private String subscriptionContractUrl;
    
    private String optionalAccountDetails;
    
    private String email_verified;
    
    private String last_name;
    
    private String user_type;
    
    private Timestamp created_at;
    
    private Timestamp updated_at;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuth0Id() {
		return auth0Id;
	}

	public void setAuth0Id(String auth0Id) {
		this.auth0Id = auth0Id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(String emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getAlternate_phone() {
		return alternate_phone;
	}

	public void setAlternate_phone(String alternate_phone) {
		this.alternate_phone = alternate_phone;
	}

	public String getOffice_phone() {
		return office_phone;
	}

	public void setOffice_phone(String office_phone) {
		this.office_phone = office_phone;
	}

	public String getPrinter_settings() {
		return printer_settings;
	}

	public void setPrinter_settings(String printer_settings) {
		this.printer_settings = printer_settings;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSubscriptionPlanId() {
		return subscriptionPlanId;
	}

	public void setSubscriptionPlanId(String subscriptionPlanId) {
		this.subscriptionPlanId = subscriptionPlanId;
	}

	public String getSubscriptionContractUrl() {
		return subscriptionContractUrl;
	}

	public void setSubscriptionContractUrl(String subscriptionContractUrl) {
		this.subscriptionContractUrl = subscriptionContractUrl;
	}

	public String getOptionalAccountDetails() {
		return optionalAccountDetails;
	}

	public void setOptionalAccountDetails(String optionalAccountDetails) {
		this.optionalAccountDetails = optionalAccountDetails;
	}

	public String getEmail_verified() {
		return email_verified;
	}

	public void setEmail_verified(String email_verified) {
		this.email_verified = email_verified;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
    
    
    

    
}
