package com.sample.security;

import static org.springframework.security.config.Customizer.withDefaults;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sample.filter.JwtAuthenticationEntryPoint;
import com.sample.filter.JwtAuthenticationFilterOnce;

@Configuration
public class SecurityConfig {
	
	@Autowired
	JwtAuthenticationEntryPoint point;

	@Autowired
	JwtAuthenticationFilterOnce filter;
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

	  http		  
      	.csrf(csrf -> csrf.disable())
      	.cors(cors -> cors.disable())
      	.authorizeHttpRequests(auth -> auth
      			  .requestMatchers("/api/public/**").permitAll()
      			  .requestMatchers("/api/private/**").authenticated()
      	.anyRequest().authenticated())
      	.httpBasic(withDefaults())
      	.exceptionHandling(ex-> ex.authenticationEntryPoint(point))

      .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
		  
		 http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
	        return http.build();
	    }
}
