package com.sample.repository.wms;

import com.sample.model.wms.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {

    Account findByAccountSlug(String accountSlug);

    @Query("SELECT a.accountSlug FROM Account a WHERE a.warehouseId != 0 AND a.warehouseId IS NOT NULL")
    List<String> getWarehouseWorkspaceAccount();
}
