package com.sample.repository.wms;

import com.sample.model.wms.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	 User findByEmail(String email);
	 
	 User findByAuth0Id(String auth0id);
}
