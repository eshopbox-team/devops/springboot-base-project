package com.sample.repository.wms;

import com.sample.model.wms.Verticals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerticalsRepository extends JpaRepository<Verticals, Long> {
}
