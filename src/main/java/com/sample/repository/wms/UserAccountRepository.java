package com.sample.repository.wms;

import com.sample.model.wms.UserAccount;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {


    UserAccount findByUserIdAndAccountId(Long userId, Long accountId);

    @Query(value = "SELECT u.account_id AS accountId, u.role AS userAccountRole " +
            "FROM user_account_mapping u " +
            "JOIN accounts a ON u.account_id = a.id " +
            "WHERE u.user_id = :userId AND a.account_slug = :accountSlug",
            nativeQuery = true)
    UserAccount getUserAccountMappingNative(@Param("accountSlug") String accountSlug, @Param("userId") Long userId);



}
